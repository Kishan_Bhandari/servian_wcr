#Do not edit this code ----
# Purpose:
#           OAth Wellview protocole with refresh and Bearer tokens to get access to API
#           Returns a json file to the calling function
# Inputs:
#          Refresh_token: exist in Refresh_token.txt (no entry - automatic update with new token for the next call to the API)
#          Well name: User to place well name as an input - search for idwell in CSV file (wells.csv)
# Example Call:
#           JsonFile = getJsons(idwellFile, idwell, well, saveJson, dataType)
#           saveJson can be set to true or false.
#           idwellFile, idwell and well - refer to config file
#       Example of: idwellFile: "df = pd.read_csv(idwellFile + 'IDWELL_WV.csv')"
#                   idwell = df.iloc[:, 0].tolist()
#                   well = df.iloc[:, 1].tolist()
#           dataType refer to the wellview data code (eg. dataType = ['wvTubComp'])
# Outputs
#          Json files for surface and production casing on define well
#
# Dependencies:
#           python.http.client
#           json
#           requests
#           pandas
#           BeautifulSoup4
#
# History:
#           06/10/2020 - initial version - GVF
#           07/10/2020 - restructure to re-use code
#           27/10/2020 - JsonReformat
#           12/11/2020 - JsonExtract
import json
import requests
from google.cloud import storage
import GetToken
import SubKeyFromSM
import re
from datetime import datetime

# ------------------
class API():
    def __init__(self,subkey,Table_list,dt_string,wellnames_passed_in='ALL_WELLS'):
        self.urlBase= 'https://api.peloton.com/v1/senex/wellview/data'
        self.wellnames_passed_in=wellnames_passed_in
        self.subkey = subkey
        self.JSONLandingBucket = 'gcp-sxy-dataplat-dev-gcs-landing'
        self.Table_list = Table_list
        print (self.Table_list)
        self.dt_string =dt_string
    def getWellidAndName(self,subkey):
        dataType = 'wvwellheader'
        subkey=subkey
        WVbearerToken = GetToken.GetToken().Main()
        url = '{urlBase}/{dataType}'.format(urlBase=self.urlBase, dataType=str(dataType))
        payload = {}
        headers = {
            'Ocp-Apim-Subscription-Key': subkey,
            'wellview': 'dwB2AHwAMQAwAC4AMAAwAHwAMQAwAC4ANAAwAHwAaQBkAHcAZQBsAGwAfAB3AHYAdwBlAGwAbABoAGUAYQBkAGUAcgB8ADEAQgA1ADUAMQAxAEIAMAAzADUANAA4ADQANQAyADYAQgAxAEEARQA5ADkAOAA0ADEAOQBCADkAMAA3ADkARgB8AFcAZQBsAGwAVgBpAGUAdwB8AFcAZQBsAGwAVgBpAGUAdwB8AHcAdgB8ADEAMAAuADQAMAA=',
            'Authorization': WVbearerToken
        }

        response = requests.request("GET", url, headers=headers, data=payload)
        if response.status_code != 200:
            print('Unsucessful WV API communications - Call Gonzalo Vazquez 0416-256-990')
            print('problems?', response.raise_for_status())

        responseJson = response.json()
        idwell_wellname = {}
        for x in responseJson:
            idwell_wellname[x['wellname']] = x['idwell']
        return idwell_wellname

    def getWellViewData(self,datum, urlBase, idwell, well, bearerToken,subkey):
        url = '{urlBase}/{dataType}/entityid/{idwell}?includecalcs=true'.format(urlBase=urlBase, idwell=idwell, dataType=datum)
        print(url)
        payload = {}
        subkey=subkey
        headers = {
            'Ocp-Apim-Subscription-Key': subkey,
            'wellview': 'dwB2AHwAMQAwAC4AMAAwAHwAMQAwAC4ANAAwAHwAaQBkAHcAZQBsAGwAfAB3AHYAdwBlAGwAbABoAGUAYQBkAGUAcgB8ADEAQgA1ADUAMQAxAEIAMAAzADUANAA4ADQANQAyADYAQgAxAEEARQA5ADkAOAA0ADEAOQBCADkAMAA3ADkARgB8AFcAZQBsAGwAVgBpAGUAdwB8AFcAZQBsAGwAVgBpAGUAdwB8AHcAdgB8ADEAMAAuADQAMAA=',
            'Authorization': bearerToken
        }
        response = requests.request("GET", url, headers=headers, data=payload)
        if response.status_code == 200:
            print('WV API comms 0: ' + well + ' - ' + datum)
        elif response.status_code != 200:
            print('WV API comms 1: ' + datum + ' - Call Gonzalo Vazquez 0416-256-990')
            print('problems?', response.raise_for_status())
        response.raise_for_status()
        responseJson = response.json()
        return responseJson

    def upload_blob_From_String(self, bucket_name,sourceString,file):
        """Uploads a file to the bucket."""
        self.storage_client = storage.Client()
        bucket = self.storage_client.get_bucket(bucket_name)
        blob = bucket.blob(file)
        # blod_Backup=bucket.blob(destination_blob_name)
        blob.upload_from_string(sourceString,content_type ='text/json')
        print('uploaded to {}.'.format(file))
    def CreateJsonFile(self,dataType,well):

        JsonFile = 'wellview/JSON/' + str(self.dt_string) + '/' + dataType + '/' + str(well) +'.json'
        print ("oooooooooooooooo",JsonFile)
        return JsonFile
    def RequestWellData(self,wellname,Table_list):
        for wellname,idwell in wellname.items():
            well=re.sub(r'\*+','',re.sub(r'\s+','',wellname))
            print ("-------------",well)
            for dataType in Table_list:
                WVbearerToken = GetToken.GetToken().Main()
                json_WV = self.getWellViewData(dataType, self.urlBase, idwell, well, WVbearerToken,self.subkey)
                print ("&&&&&&&&&&",json_WV)
                json_WV=json.dumps(json_WV)
                #Jsondir = 'JsonFiles_' + str(well) gcp-sxy-dataplat-dev-gcs-landing/wellview
                yield json_WV,dataType,well

    def ParseInputParams(self,idwell_name_dict):
        onetime_wellname_id={}
        #if len(sys.argv) > 1: #means there is parameters passed in
        if self.wellnames_passed_in!='ALL_WELLS':
            #well_names = sys.argv[1].split(',') #take the second parameter as the well names list. the parameter can be 'Altas-01' or 'Altas-01,Atlas-02,***Sample-Well' which uses comma to separate.
            well_names = self.wellnames_passed_in.split(',')
            for one_well_name in well_names:
                if one_well_name in idwell_name_dict.keys():
                    print (one_well_name + ' Matched')
                    onetime_wellname_id[one_well_name] = idwell_name_dict[one_well_name] #here is another dict , well name: id key-value pair dict
                else:
                    print (one_well_name + ' NOT Matched please check well name input')

        if self.wellnames_passed_in=='ALL_WELLS':
            onetime_wellname_id = idwell_name_dict#take idwell_name_dict as the input parameters
        return onetime_wellname_id


    def Main_DownloadFromWv(self):
        idwell_name_dict = self.getWellidAndName(self.subkey)
        well_name_id_queried = self.ParseInputParams(idwell_name_dict)
        for json_WV,dataType,well in self.RequestWellData(well_name_id_queried,self.Table_list):
            JsonFile=self.CreateJsonFile(dataType,well)
            self.upload_blob_From_String(self.JSONLandingBucket,json_WV,JsonFile)

if __name__=='__main__':
    SubKey=SubKeyFromSM.GetSubKey().Main()
    api=API(SubKey,'****Sample Well,Atlas-1,Glenora-33')
    api.Main_DownloadFromWv()
