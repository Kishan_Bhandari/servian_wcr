import SubKeyFromSM
#import GetToken
import APIToWV
import QueryBQ
from datetime import datetime
import MergeJsonToCSV
#from logger import logger
class WVPipeLine():
    def __init__(self):
        self.PROJECT_ID = "gcp-sxy-dataplat-dev"
        self.GetWVColumnFromBQ = "wellview_config.wellview_source_table_columns"
        self.GetTableListFromBQ = 'wellview_config.wellview_source_tables'
        # dd/mm/YY
        self.dt_string = datetime.utcnow().strftime("%Y%m%d")
        print ('Current Date: ',self.dt_string)
        #logger.info('Current Date: ',self.dt_string)
        self.QueryBQ = QueryBQ.QueryBQ()
        self.Table_list = self.QueryBQ.GetSourceTableList(self.PROJECT_ID,self.GetTableListFromBQ)
        #print (self.Table_list)
        # self.Table_list = {
        #     'wvWellheader'
        #     , 'wvJob'
        #     , 'wvCas'
        #     , 'wvCasComp'
        #     , 'wvTubComp'
        #     , 'wvJCostSumDailyDesCalc'
        #     ,'wvCement'
        # }
        self.SubKey = SubKeyFromSM.GetSubKey().Main()
        self.APIToWV=APIToWV.API(self.SubKey,self.Table_list,self.dt_string,'****Sample Well,Atlas-1,Glenora-33')



    def Main(self):
        ColumnsMapping = {}
        self.APIToWV.Main_DownloadFromWv()
        for table in self.Table_list:

            #self.QueryBQ = QueryBQ.QueryBQ(self.PROJECT_ID,self.BQTable)
            Cols = self.QueryBQ.GetTableColumns((''.join(table)).lower(),self.PROJECT_ID,self.GetWVColumnFromBQ)
            ColumnsMapping[table.lower()]=Cols  #make all tables to lower case, to make sure is case insensitive
        #print (ColumnsMapping)
        self.MergeJsonToCSV=MergeJsonToCSV.MergeJsonToCSV(ColumnsMapping,self.dt_string)
        self.MergeJsonToCSV.Main()
if __name__ == '__main__':
    wv=WVPipeLine()
    wv.Main()