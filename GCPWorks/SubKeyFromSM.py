import json
import logging
import pandas as pd
import datetime as dt
from google.cloud import bigquery
from google.cloud import secretmanager
from google.cloud import error_reporting
from google.cloud.exceptions import GoogleCloudError, NotFound
from logger import logger

class GetSubKey():
    def __init__(self):
        self.Project_ID = 'gcp-sxy-dataplat-dev'
        self.Secret_ID = 'gcp-sxy-dataplat-dev-wellview-api-secrets'
    def access_secret_value(self,project_id: str, secret_id: str, version_id="latest"):
        try:
            print(f"Getting secret value from Secrets Manager for {secret_id}")
            #logger.info(f"Getting secret value from Secrets Manager for {secret_id}", icon="📋", )
            # Create the Secrets Manager client.
            client = secretmanager.SecretManagerServiceClient()

            # Build the resource name of the secret version.
            name = f"projects/{project_id}/secrets/{secret_id}/versions/{version_id}"

            # Access the secret version. Default version is "latest"
            response = client.access_secret_version(request={"name": name})
            secret_value = response.payload.data.decode("UTF-8")

            if secret_value != "":
                print(f"secret value found for {secret_id}")
                #logger.info(f"secret value found for {secret_id}", icon="💌")

        except (ValueError, NotFound) as e:
            #logger.error(e)
            print("secret value NOT found for {secret_id}")
            raise NotFound(f"secret value not found for {secret_id}")

        return secret_value

    def Main(self):
        secVal = self.access_secret_value(self.Project_ID,self.Secret_ID)
        sv = json.loads(secVal)
        subkey = sv["subkey"]
        print("THE OUTPUT IS: " + subkey)
        return subkey
if __name__ == '__main__':
    a=GetSubKey()
    a.Main()