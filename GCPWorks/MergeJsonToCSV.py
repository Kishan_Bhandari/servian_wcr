# This is a sample Python script.
import os
import re
import pandas as pd
from google.cloud import storage
import json
import ast
#from logger import logger
class MergeJsonToCSV():
    def __init__(self,Cols_Dict,dt_string):
        self.JsonLocation = 'wellview/JSON/Glenora-33/wvCas.js'
        self.JSONLandingBucket = 'gcp-sxy-dataplat-dev-gcs-landing'
        self.CSVLandingBucket = 'gcp-sxy-dataplat-dev-gcs-landing'
        self.Cols_Dict = Cols_Dict
        self.dt_string=dt_string
        self.storage_client = storage.Client()
        self.Json_bucket = self.storage_client.get_bucket(self.JSONLandingBucket)
        self.CSV_bucket = self.storage_client.get_bucket(self.CSVLandingBucket)
    def list_blobs(self,bucket_name):
        """Lists all the blobs in the bucket."""
        storage_client = storage.Client()
        blobs = storage_client.list_blobs(bucket_name,prefix='wellview/JSON/'+self.dt_string)
        return blobs

    def ConvertJsonToDF(self,blobs):  #convert each json to DF and merge them
        pd.set_option('display.max_columns',None)
        frame={}
        for blob in blobs:
            print(blob.name)
            ParseBlob = re.search(r'\d+/(\S+)/(\S+).json',str(blob))
            if ParseBlob is not None:
                wvTableName = ParseBlob.group(1)
                print ("------",wvTableName)
                WellName = ParseBlob.group(2)
                JsonBlob = self.Json_bucket.blob(str(blob.name))
                JsonContent = JsonBlob.download_as_string().decode("UTF-8")
                if wvTableName in frame.keys():
                    frame[wvTableName] += ast.literal_eval(JsonContent)
                else:
                    frame[wvTableName] = ast.literal_eval(JsonContent)
        for TableName, TableRecs in frame.items():
            df_Frame = []
            for one_rec in TableRecs:
                df_Frame.append(pd.DataFrame.from_dict([one_rec]))
            if len(df_Frame)>0:
                MergedDF = pd.concat(df_Frame)
            else:
                MergedDF = pd.DataFrame.from_dict([{}])
            yield TableName, MergedDF
    def CreateCSV(self,TableName,MergedJsonDF):
        if MergedJsonDF.empty:
            print (TableName," is empty")
            #logger.info(TableName," is empty",icon="📋",)
        else:
            TableNameLow = TableName.lower()
            if TableNameLow in self.Cols_Dict.keys():  #lower table name, since keys in self.Cols_Dict are lower case (lowered when passing from WVPipeLine.py)
                Table_Columns = self.Cols_Dict[TableNameLow].split(',')
                df_to_upload = pd.DataFrame(columns=Table_Columns)
                for col in Table_Columns:
                    if col.lower() in MergedJsonDF.keys():
                        df_to_upload[col] = MergedJsonDF[col.lower()].fillna('')
                    else:
                        df_to_upload[col] = ''
            else:
                df_to_upload=pd.DataFrame.from_dict([{}])
            CSVBlob=self.CSV_bucket.blob('peloton/wellview/'+TableName+'/'+TableName+'.csv')
            try:
                CSVBlob.upload_from_string(df_to_upload.to_csv(index=False),'text/csv')
                #logger.info('peloton/wellview/'+TableName+'/'+TableName+'.csv',' created Successfully', icon="📋", )
                print ('peloton/wellview/'+TableName+'/'+TableName+'.csv',' created Successfully')
            except Exception as e:
                #logger.info(e)
                #logger.info('peloton/wellview/'+TableName+'/'+TableName+'.csv', ' FAIL TO CREATE!')
                print ('peloton/wellview/'+TableName+'/'+TableName+'.csv', ' FAIL TO CREATE!')

    def Main(self):
        blobs=self.list_blobs(self.JSONLandingBucket)
        for TableName, MergedDF in self.ConvertJsonToDF(blobs):
            self.CreateCSV(TableName, MergedDF)

if __name__ == '__main__':
    a=MergeJsonToCSV()
    a.Main()