from logger import logger
import pandas
from google.cloud import bigquery

class QueryBQ():
    def __init__(self):
        pass
        #self.PROJECT_ID = "gcp-sxy-dataplat-dev"
        #self.BQTable = "wellview_config.wellview_source_table_columns"
        #self.PROJECT_ID = Project_ID
        #self.BQTable = BQTable
        #self.TableName = TableName
    def GetSourceTableList(self,PROJECT_ID,BQTable):
        bqclient = bigquery.Client(project=PROJECT_ID)
        query_string = f"SELECT source_table_name FROM `{PROJECT_ID}.{BQTable}` WHERE active_flag = 'Y'"
        df = bqclient.query(query_string).result().to_dataframe()
        #print (df)
        return df.values
    def GetTableColumns(self,TableName,PROJECT_ID,BQTable):
        bqclient = bigquery.Client(project=PROJECT_ID)
        query_string = f"SELECT column_names FROM `{PROJECT_ID}.{BQTable}` WHERE lower(source_table_name) = '" + TableName + "'"
        df = bqclient.query(query_string).result().to_dataframe().to_dict('records')
        if len(df) > 0:
            Cols=df[0]['column_names']
        else:
            Cols=''
        return Cols
