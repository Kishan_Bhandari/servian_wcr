import http.client
import json
import os
from google.cloud import storage
#from logger import logger
#from google.cloud import secretmanager
#from google.cloud.exceptions import GoogleCloudError, NotFound
class GetToken():
    def __init__(self):
        self.Token_bucket_name = 'gcp-sxy-dataplat-dev-gcs-source-system-config'
        self.Token_File = 'wellview_config/Refresh_token.txt'
        self.BackUp_Token_File = 'wellview_config/Refresh_token_Backup.txt'
        self.storage_client = storage.Client()
        self.Token_bucket = self.storage_client.get_bucket(self.Token_bucket_name)
    def ReadCurrentToken(self,Token_File):
        #1fe53b876b0082a4f843b3886358b2e0205e7946d89d8a4631065f101ef8f136
        #96483c3fd288aa708e82a804e70d3980ee677a6d6fa8e800fd2d8594cec56422
        Token_Blob = self.Token_bucket.blob(Token_File)
        Current_Token = Token_Blob.download_as_string().decode("UTF-8")
        print (Current_Token)
        return Current_Token
    def CreateRefreshedToken(self,CurrentToken):
        conn = http.client.HTTPSConnection("id.peloton.com")
        Token_Request = [
            'grant_type=refresh_token&client_id=appframe-svc-api-authcode&scope=appframe-svc-api-authcode%20offline_access%20openid%20profile&refresh_token=',CurrentToken]
        call_refresh_token = ''.join(Token_Request)
        payload = call_refresh_token
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
        }
        conn.request("POST", "/connect/token", payload, headers)
        res = conn.getresponse()
        Token_Res = res.read()
        print(Token_Res.decode("utf-8")) #leave here just in case refresh token or id_token is lost
        conn.close()
        resDecoded = Token_Res.decode("utf-8")
        resJson = json.loads(resDecoded)
        TokenDict = dict(resJson)
        Refreshed_Token = (TokenDict['refresh_token'])
        print (Refreshed_Token)
        bearer = (TokenDict['access_token'])
        bearerString = ['bearer', bearer]
        bearerToken = ' '.join(bearerString)
        return Refreshed_Token,bearerToken

    def upload_blob_From_String(self, bucket_name,sourceString,file):
        """Uploads a file to the bucket."""
        self.storage_client = storage.Client()
        bucket = self.storage_client.get_bucket(bucket_name)
        blob = bucket.blob(file)
        # blod_Backup=bucket.blob(destination_blob_name)
        blob.upload_from_string(sourceString)
        print('Token key uploaded to {}.'.format(file))
    """
    def access_secret_value(self,project_id: str, secret_id: str, version_id="latest"):
        try:
            print(f"Getting secret value from Secrets Manager for {secret_id}")
            logger.info(f"Getting secret value from Secrets Manager for {secret_id}", icon="📋", )
            # Create the Secrets Manager client.
            client = secretmanager.SecretManagerServiceClient()

            # Build the resource name of the secret version.
            name = f"projects/{project_id}/secrets/{secret_id}/versions/{version_id}"

            # Access the secret version. Default version is "latest"
            response = client.access_secret_version(request={"name": name})
            secret_value = response.payload.data.decode("UTF-8")

            if secret_value != "":
                print(f"secret value found for {secret_id}")
                logger.info(f"secret value found for {secret_id}", icon="💌")

        except (ValueError, NotFound) as e:
            logger.error(e)
            print("secret value NOT found for {secret_id}")
            raise NotFound(f"secret value not found for {secret_id}")

        return secret_value

    def add_secret_version(self,PROJECT_ID, secret_id, payload):
        # Create the Secret Manager client.
        client = secretmanager.SecretManagerServiceClient()

        # Build the resource name of the parent secret.
        parent = f"projects/{PROJECT_ID}/secrets/{secret_id}"

        # Convert the string payload into a bytes. This step can be omitted if you
        # pass in bytes instead of a str for the payload argument.
        payload = payload.encode('UTF-8')

        # Add the secret version.
        response = client.add_secret_version(parent=parent, payload={'data' : payload})

        # Print the new secret version name.
        print(f'Added secret version: {response.name}')
        """
    def Main(self):
        Current_Token=self.ReadCurrentToken(self.Token_File)
        try:
            Refreshed_Token,bearerToken = self.CreateRefreshedToken(Current_Token)
        except:
            print ('Current Token Invalid:',Current_Token,' trying to read Refresh_token_Backup.txt')
            Backup_Token = self.ReadCurrentToken(self.BackUp_Token_File)
            Refreshed_Token, bearerToken = self.CreateRefreshedToken(Backup_Token)
        self.upload_blob_From_String(self.Token_bucket_name,Refreshed_Token,self.Token_File)
        self.upload_blob_From_String(self.Token_bucket_name,Refreshed_Token,self.BackUp_Token_File)
        return bearerToken

        #Current_Token = self.access_secret_value("gcp-sxy-dataplat-dev","gcp-sxy-dataplat-dev-wellview-api-secrets_refreshed_token")
        #print (Current_Token)
        #self.add_secret_version("584036728544","gcp-sxy-dataplat-dev-wellview-api-secrets_refreshed_token","hello")

if __name__ == '__main__':
    a=GetToken()
    a.Main()

# def createfile():
#     f=open('./derekTest.txt','w+')
#     f.write('testtesttest12345')
#     f.close()


# def upload_blob(bucket_name, source_file_name, destination_blob_name):
#   """Uploads a file to the bucket."""
#   storage_client = storage.Client()
#   bucket = storage_client.get_bucket(bucket_name)
#   blob = bucket.blob(destination_blob_name)
#
#   blob.upload_from_filename(source_file_name)
#
#   print('File {} uploaded to {}.'.format(
#       source_file_name,
#       destination_blob_name))
#createfile()
#storage_client =storage.Client()
#print(storage_client)
#gcp-sxy-dataplat-dev-gcs-landing/wellview/Token


#upload_blob_From_String('gcp-sxy-dataplat-dev-gcs-landing','1fe53b876b0082a4f843b3886358b2e0205e7946d89d8a4631065f101ef8f136','wellview/Token/Refresh_token.txt')