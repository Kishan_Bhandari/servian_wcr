import logging
from google.cloud import logging  as google_cloud_logging     # Imports the Cloud Logging client library

cloud_logging_client = google_cloud_logging.Client()

# Integrates Cloud Logging handler with Python logging module
cloud_logging_client.get_default_handler()
cloud_logging_client.setup_logging()

# ------------------------------- Custom logger ------------------------------ #

LOG_ICONS = {
    logging.CRITICAL: "🔥",
    logging.ERROR: "🛑",
    logging.WARNING: "⚠️",
    logging.INFO: "📘",
    logging.DEBUG: "🧪",
}


class IconFormatter(logging.Formatter):
    def __init__(self):
        fmt = "{icon} {message}"
        super().__init__(fmt=fmt, datefmt=None, style="{")

    def format(self, record: logging.LogRecord):
        record.icon = getattr(record, "icon", None) or LOG_ICONS.get(
            record.levelno, "ℹ️"
        )
        return logging.Formatter.format(self, record)


class ContextAdpater(logging.LoggerAdapter):
    def __init__(self, logger, extra=None):
        self.context = []
        return super().__init__(logger, extra or {})

    def set_context(self, *args):
        self.context = [a for a in args if a is not None]

    def process(self, msg, kwargs):
        extra = kwargs.pop("extra", {})
        extra["icon"] = kwargs.pop("icon", None)
        kwargs["extra"] = extra

        if len(self.context) > 0:
            msg = f'[{"/".join(self.context)}] {msg}'

        return msg, kwargs


formatter = IconFormatter()
_logger = logging.getLogger()

for handler in _logger.handlers:
    handler.setFormatter(formatter)

logger = ContextAdpater(_logger)
