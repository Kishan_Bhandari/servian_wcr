import json
import requests
import pandas as pd
import http.client
import socket

#-------

def createToken():
    #Reads refresh token from folder
    file = r"I:\00. Temporary File Dump Location\Wellview API\Refresh_token.txt"
    file_object_refresh_token = open(file).read()
    refresh_token = file_object_refresh_token
    conn = http.client.HTTPSConnection("id.peloton.com")
    strings_1 = [
        'grant_type=refresh_token&client_id=appframe-svc-api-authcode&scope=appframe-svc-api-authcode%20offline_access%20openid%20profile&refresh_token=',
        refresh_token]
    call_refresh_token = ''.join(strings_1)
    payload = call_refresh_token
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
    }
    conn.request("POST", "/connect/token", payload, headers)
    res = conn.getresponse()
    data = res.read()
    # print(data.decode("utf-8")) leave here just in case refresh token or id_token is lost

    conn.close()

    resDecoded = data.decode("utf-8")
    resJson = json.loads(resDecoded)
    values = dict(resJson)

    refreshToken = (values['refresh_token'])

    with open(file, 'w') as file:
        file.write(refreshToken)

    bearer = (values['access_token'])
    bearerString = ['bearer', bearer]
    bearerToken = ' '.join(bearerString)

    return bearerToken

def getWellViewData(dataType):

    url = '{urlBase}/{dataType}/entityid/{idwell}?includeCalcs=True'.format(urlBase=urlBase, idwell=IDWELL, dataType=dataType)
    payload = {}
    #url = 'https://api.peloton.com/v1/senex/wellview/data/wvwellheader/entityid/4BA45C2C431E49EABF651E5ED2513723/recordid/4BA45C2C431E49EABF651E5ED2513723'

    headers = {
        'Ocp-Apim-Subscription-Key': getsubkey(),
        'wellview': 'dwB2AHwAMQAwAC4AMAAwAHwAMQAwAC4ANAAwAHwAaQBkAHcAZQBsAGwAfAB3AHYAdwBlAGwAbABoAGUAYQBkAGUAcgB8ADEAQgA1ADUAMQAxAEIAMAAzADUANAA4ADQANQAyADYAQgAxAEEARQA5ADkAOAA0ADEAOQBCADkAMAA3ADkARgB8AFcAZQBsAGwAVgBpAGUAdwB8AFcAZQBsAGwAVgBpAGUAdwB8AHcAdgB8ADEAMAAuADQAMAA=',
        'Authorization': WVbearerToken
    }



    response = requests.request("GET", url, headers=headers, data=payload)

    if response.status_code != 200:
        print('Unsucessful WV API communications - Call Gonzalo Vazquez 0416-256-990')
        print('problems?', response.raise_for_status())
#Taking the first element of the Json list to tranform to a dictionary

    responseJson = response.json()
    return responseJson


def getsubkey():
    if socket.gethostname() == 'senazedigitalprod01':
        subkey = '1e1759a462ae49719652778eb8c7bc92'
    elif socket.gethostname() == 'senazewvinttest01':
        subkey = '0c3286528c55439da512e07d16f91970'
    else:
        subkey = '3b2d9094f1d8415f9bcfc7615565112b'
    return subkey
def PutWellViewData(dataType, payload):
    url = '{urlBase}/{dataType}/'.format(urlBase=urlBase, dataType=dataType)

    #This is the format for Wellview Payload string and double quoate in each field --- tested 22-01-2021
    #payload = '{"idwell":"31CB5F85EBD9474FBAD0A3E5AF344D31", "Country":"im testing 20210720"}'
    #transform payload to Wellview json like file as above
    subkey=getsubkey()
    payload = json.dumps(payload[0])
    print (payload)
    #lesson learn: when using PUT function for wellview always specify Content-TYPE as application/json
    headers = {
        'Ocp-Apim-Subscription-Key': subkey,
        'wellview': 'dwB2AHwAMQAwAC4AMAAwAHwAMQAwAC4ANAAwAHwAaQBkAHcAZQBsAGwAfAB3AHYAdwBlAGwAbABoAGUAYQBkAGUAcgB8ADEAQgA1ADUAMQAxAEIAMAAzADUANAA4ADQANQAyADYAQgAxAEEARQA5ADkAOAA0ADEAOQBCADkAMAA3ADkARgB8AFcAZQBsAGwAVgBpAGUAdwB8AFcAZQBsAGwAVgBpAGUAdwB8AHcAdgB8ADEAMAAuADQAMAA=',
        'Authorization': WVbearerToken,
        'Content-Type': 'application/json'
    }

    response = requests.request("PUT", url, data=payload, headers=headers)
    #response = requests.put(url, data=payload, headers=headers)
    print('Response from Wellview PUT', response)
    if response.status_code != 200:
        print('Unsucessful WV API communications - Call Gonzalo Vazquez 0416-256-990')
        print('problems?', response.raise_for_status())


#--------------------------------------------------
#*****Sample well P&A
#IDWELL = 'C280EC27E29B4662828206B193AFF7CC'
#*****Sample well
#IDWELL = '31CB5F85EBD9474FBAD0A3E5AF344D31'

#create bearer token from wellview
WVbearerToken = createToken()
urlBase = 'https://api.peloton.com/v1/senex/wellview/data'

dataType = 'wvWellHeader'
#LayerName
#json_WV=getWellViewData(dataType)
#json_WV[0]['UserNum6']='7.21'
#print (json_WV)
# json_WV[0]['country']="i'm a testing @ 20210720"
# #json_WV[0]['UserNum6']='im testing 20210720'
# print (json_WV)
# json_new={}
# json_new["idwell"] = json_WV[0]['idwell']
# json_new["country"]='im a testing @ 20210720'
# json_new['UserNum6']='Senex integration test @ 20210720'
#Walloon Coal Measures
#payload = [{"idwell":["31CB5F85EBD9474FBAD0A3E5AF344D31","C280EC27E29B4662828206B193AFF7CC"], "Country":["im testing 20210723_SW1", "im testing 20210723-PA"]}]
payload = [{"idwell":"31CB5F85EBD9474FBAD0A3E5AF344D31999", "coUNtry":'testnew2'}]
#requests.exceptions.HTTPError: 404 Client Error: Not Found for url

PutWellViewData(dataType, payload)



# dataType = 'wvwellheader'
# PutWellViewData(dataType, json_WV)
#'latitude': -26.172584,