#Do not edit this code ----
# Purpose:
#           OAth Wellview protocole with refresh and Bearer tokens to get access to API
#           Returns a json file to the calling function
# Inputs:
#          Refresh_token: exist in Refresh_token.txt (no entry - automatic update with new token for the next call to the API)
#          Well name: User to place well name as an input - search for idwell in CSV file (wells.csv)
# Example Call:
#           JsonFile = getJsons(idwellFile, idwell, well, saveJson, dataType)
#           saveJson can be set to true or false.
#           idwellFile, idwell and well - refer to config file
#       Example of: idwellFile: "df = pd.read_csv(idwellFile + 'IDWELL_WV.csv')"
#                   idwell = df.iloc[:, 0].tolist()
#                   well = df.iloc[:, 1].tolist()
#           dataType refer to the wellview data code (eg. dataType = ['wvTubComp'])
# Outputs
#          Json files for surface and production casing on define well
#
# Dependencies:
#           python.http.client
#           json
#           requests
#           pandas
#           BeautifulSoup4
#
# History:
#           06/10/2020 - initial version - GVF
#           07/10/2020 - restructure to re-use code
#           27/10/2020 - JsonReformat
#           12/11/2020 - JsonExtract

import http.client
import json
import requests
import os
import pandas as pd
from bs4 import BeautifulSoup
import socket
import sys
import re
# ------------------
class API():
    def __init__(self,wellnames_passed_in='ALL_WELLS'):
        self.urlBase= 'https://api.peloton.com/v1/senex/wellview/data'
        self.wellnames_passed_in=wellnames_passed_in
    def getsubkey(self):
        if socket.gethostname() == 'senazedigitalprod01':
            subkey = '1e1759a462ae49719652778eb8c7bc92'
        elif socket.gethostname() == 'senazewvinttest01':
            subkey = '0c3286528c55439da512e07d16f91970'
        else:
            subkey = '3b2d9094f1d8415f9bcfc7615565112b'
        return subkey

    def getidwellfile(self):
        # Read config file with wellID and refresh token
        #configpath = str(Path(Path.cwd()).parents[0]) + r'/ops/Drilling/WellView/config.xml'
        configpath = str('.\\config.xml')
        with open(configpath) as configFile:
            content = configFile.read()
            content = BeautifulSoup(content, 'lxml')
            # Identify the machine running the program (dev/test/prod)
            if socket.gethostname() == 'senazedigitalprod01':
                idwellFile = content.config.filelocation.wellstokenprod.string  #Prod
            elif socket.gethostname() == 'senazewvinttest01':
                idwellFile = content.config.filelocation.wellstokenprod.string  #Test
            else:
                idwellFile = content.config.filelocation.wellstokendev.string   # Dev
        return idwellFile

    def createToken(self,idwellFile):
        file = idwellFile + 'Refresh_token.txt'
        file_object_refresh_token = open(file).read()
        refresh_token = file_object_refresh_token

        conn = http.client.HTTPSConnection("id.peloton.com")
        strings_1 = [
            'grant_type=refresh_token&client_id=appframe-svc-api-authcode&scope=appframe-svc-api-authcode%20offline_access%20openid%20profile&refresh_token=',
            refresh_token]
        call_refresh_token = ''.join(strings_1)
        payload = call_refresh_token
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
        }
        conn.request("POST", "/connect/token", payload, headers)
        res = conn.getresponse()
        data = res.read()
        #print(data.decode("utf-8")) #leave here just in case refresh token or id_token is lost

        conn.close()

        resDecoded = data.decode("utf-8")
        resJson = json.loads(resDecoded)
        values = dict(resJson)
        #print (values)
        refreshToken = (values['refresh_token'])

        with open(file, 'w') as file:
            file.write(refreshToken)

        bearer = (values['access_token'])
        bearerString = ['bearer', bearer]
        bearerToken = ' '.join(bearerString)

        return bearerToken


    # -----------------------------
    def getWellidAndName(self,dataType):
        subkey=self.getsubkey()
        idwellFile=self.getidwellfile()
        WVbearerToken = self.createToken(idwellFile)
        url = '{urlBase}/{dataType}'.format(urlBase=self.urlBase, dataType=str(dataType))
        payload = {}
        headers = {
            'Ocp-Apim-Subscription-Key': subkey,
            'wellview': 'dwB2AHwAMQAwAC4AMAAwAHwAMQAwAC4ANAAwAHwAaQBkAHcAZQBsAGwAfAB3AHYAdwBlAGwAbABoAGUAYQBkAGUAcgB8ADEAQgA1ADUAMQAxAEIAMAAzADUANAA4ADQANQAyADYAQgAxAEEARQA5ADkAOAA0ADEAOQBCADkAMAA3ADkARgB8AFcAZQBsAGwAVgBpAGUAdwB8AFcAZQBsAGwAVgBpAGUAdwB8AHcAdgB8ADEAMAAuADQAMAA=',
            'Authorization': WVbearerToken
        }

        response = requests.request("GET", url, headers=headers, data=payload)
        if response.status_code != 200:
            print('Unsucessful WV API communications - Call Gonzalo Vazquez 0416-256-990')
            print('problems?', response.raise_for_status())

        responseJson = response.json()
        return responseJson

    def getWellViewData(self,datum, urlBase, idwell, well, bearerToken):
        url = '{urlBase}/{dataType}/entityid/{idwell}?includecalcs=true'.format(urlBase=urlBase, idwell=idwell, dataType=datum)
        print(url)
        payload = {}
        subkey=self.getsubkey()
        headers = {
            'Ocp-Apim-Subscription-Key': subkey,#'3b2d9094f1d8415f9bcfc7615565112b',
            'wellview': 'dwB2AHwAMQAwAC4AMAAwAHwAMQAwAC4ANAAwAHwAaQBkAHcAZQBsAGwAfAB3AHYAdwBlAGwAbABoAGUAYQBkAGUAcgB8ADEAQgA1ADUAMQAxAEIAMAAzADUANAA4ADQANQAyADYAQgAxAEEARQA5ADkAOAA0ADEAOQBCADkAMAA3ADkARgB8AFcAZQBsAGwAVgBpAGUAdwB8AFcAZQBsAGwAVgBpAGUAdwB8AHcAdgB8ADEAMAAuADQAMAA=',
            'Authorization': bearerToken
        }

        response = requests.request("GET", url, headers=headers, data=payload)

        if response.status_code == 200:
            print('WV API comms 0: ' + well + ' - ' + datum)
        elif response.status_code != 200:
            print('WV API comms 1: ' + datum + ' - Call Gonzalo Vazquez 0416-256-990')
            print('problems?', response.raise_for_status())

        response.raise_for_status()

        responseJson = response.json()

        # responseJson = json.dumps(json_file, sort_keys=True, indent=4) Change Json to format to see it un columns with pandas in a data set -- GVF

        return responseJson

    def RequestWellHeader(self):
        dataType = 'wvwellheader'

        json_WellHeader = self.getWellidAndName(dataType)
        idwell_wellname = {}
        for x in json_WellHeader:
            idwell_wellname[x['wellname']]=x['idwell']
        return idwell_wellname

    def RequestWellData(self,wellname):
        JsonPath = './JsonFilesNewAPITest/'
        Table_list = {
             'wvWellheader'
            ,'wvJobReportMudVol'
           ,'wvWellboreFormation'
            , 'wvWellboreKeyDepth'
            , 'wvJobRig'
            ,'wvJob'
           ,'wvWellbore'
            , 'wvWellboreSize'
            , 'wvJobDrillString'
            #  ,'wvJobBit'  # requests.exceptions.HTTPError: 400 Client Error: Bad Request: The item 'wvJobBit' is not found. for url: https://api.peloton.com/v1/senex/wellview/data/wvJobBit/entityid/B1CAD75BB75E436098F5A7E3FABE7A01
            , 'wvJobDrillBit'
            , 'wvJobDrillStringDrillParam'
            , 'wvTub'
            , 'wvCement'
            , 'wvCementStageFluid'
            , 'wvCementStage'
            , 'wvPerforation'
            , 'wvCasCompTally'
            ,'wvCas'
            , 'wvJobReportMudChk'
             , 'wvJobMudAdd'
             ,'wvWellTestRFT'
             ,'wvWellTestRFTData'
             , 'wvWellboreReservoir'
             ,'wvCasComp'
            , 'wvTubComp'
            , 'wvRodCompPCPRotor'
            , 'wvTubCompPCPStator'
            , 'wvRodComp'
            , 'wvProblemDetail'
            , 'wvJobProgramMud'
            , 'wvCementStageFluidAdd'
            , 'wvWellTestTrans'
            , 'wvWellTestTransFlowPer'
            , 'wvWellTestTransGauge'
            , 'wvWellTestTransDST'
            , 'wvJobReport'
        }  # list of distinct name from Wellview Data Mapping_Rev1_19052021 (1),column E 'WELLVIEW TABLE KEY'
        for wellname,idwell in wellname.items():
            well=wellname.replace("*",'')
            for dataType in Table_list:
                idwellFile=self.getidwellfile()
                WVbearerToken = self.createToken(idwellFile)
                json_WV = self.getWellViewData(dataType, self.urlBase, idwell, well, WVbearerToken)
                Jsondir = 'JsonFiles_' + str(re.sub(' ' , '_', well))
                if not os.path.exists(JsonPath+Jsondir):
                    os.makedirs(JsonPath+Jsondir)
                with open(JsonPath+Jsondir + '/Data' + str(dataType) + '.js', 'w') as json_file:
                    json.dump(json_WV, json_file)
                    #print(json_WV)
                print (dataType,' Downloaded !')
    def ParseInputParams(self,idwell_name_dict):
        onetime_wellname_id={}
        #if len(sys.argv) > 1: #means there is parameters passed in
        if self.wellnames_passed_in!='ALL_WELLS':
            #well_names = sys.argv[1].split(',') #take the second parameter as the well names list. the parameter can be 'Altas-01' or 'Altas-01,Atlas-02,***Sample-Well' which uses comma to separate.
            well_names = self.wellnames_passed_in.split(',')
            for one_well_name in well_names:
                if one_well_name in idwell_name_dict.keys():
                    print (one_well_name + ' Matched')
                    onetime_wellname_id[one_well_name] = idwell_name_dict[one_well_name] #here is another dict , well name: id key-value pair dict
                else:
                    print (one_well_name + ' NOT Matched please check well name input')

        if self.wellnames_passed_in=='ALL_WELLS':
            onetime_wellname_id = idwell_name_dict#take idwell_name_dict as the input parameters
        return onetime_wellname_id

    def UpdateWellViewData(self,dataType, payload):
        idwellFile = self.getidwellfile()
        WVbearerToken = self.createToken(idwellFile)
        url = '{urlBase}/{dataType}/'.format(urlBase=self.urlBase, dataType=dataType)

        # This is the format for Wellview Payload string and double quoate in each field --- tested 22-01-2021
        # payload = '{"idwell":"4BA45C2C431E49EABF651E5ED2513723", "country":"Australia"}'
        # transform payload to Wellview json like file as above

        #payload = json.dumps(payload)
        payload = payload

        # lesson learn: when using PUT function for wellview always specify Content-TYPE as application/json
        headers = {
            'Ocp-Apim-Subscription-Key': self.getsubkey(),#,'3b2d9094f1d8415f9bcfc7615565112b',
            'wellview': 'dwB2AHwAMQAwAC4AMAAwAHwAMQAwAC4ANAAwAHwAaQBkAHcAZQBsAGwAfAB3AHYAdwBlAGwAbABoAGUAYQBkAGUAcgB8ADEAQgA1ADUAMQAxAEIAMAAzADUANAA4ADQANQAyADYAQgAxAEEARQA5ADkAOAA0ADEAOQBCADkAMAA3ADkARgB8AFcAZQBsAGwAVgBpAGUAdwB8AFcAZQBsAGwAVgBpAGUAdwB8AHcAdgB8ADEAMAAuADQAMAA=',
            'Authorization': WVbearerToken,
            'Content-Type': 'application/json'
        }

        response = requests.request("PUT", url, data=payload, headers=headers)
        print('Response from Wellview PUT', response)
        if response.status_code != 200:
            print('Unsucessful WV API communications - Call Gonzalo Vazquez 0416-256-990')
            print('problems?', response.raise_for_status())

    def POSTWellViewData(self,dataType,payload):
        idwellFile = self.getidwellfile()
        bearerToken = self.createToken(idwellFile)
        url = '{urlBase}/{dataType}/'.format(urlBase=self.urlBase, dataType=dataType)
        # payload = '{"idwell":"31CB5F85EBD9474FBAD0A3E5AF344D31","LayerName":"Derek is digging2","idrecparent":"A2D711BBED53441F99FACEE21C967A1E"}'
        payload = payload
        subkey = self.getsubkey()
        headers = {
            'Ocp-Apim-Subscription-Key': subkey,  # '3b2d9094f1d8415f9bcfc7615565112b',
            'wellview': 'dwB2AHwAMQAwAC4AMAAwAHwAMQAwAC4ANAAwAHwAaQBkAHcAZQBsAGwAfAB3AHYAdwBlAGwAbABoAGUAYQBkAGUAcgB8ADEAQgA1ADUAMQAxAEIAMAAzADUANAA4ADQANQAyADYAQgAxAEEARQA5ADkAOAA0ADEAOQBCADkAMAA3ADkARgB8AFcAZQBsAGwAVgBpAGUAdwB8AFcAZQBsAGwAVgBpAGUAdwB8AHcAdgB8ADEAMAAuADQAMAA=',
            'Authorization': bearerToken
        }

        response = requests.request("POST", url, headers=headers, data=payload)
        print(response)
        if response.status_code == 201:
            print('WV API comms 0: OK')
        elif response.status_code != 201:
            print('WV API comms 1:')
            print('problems?', response.raise_for_status())

        response.raise_for_status()

        responseJson = response.json()
        return responseJson
    def Main_DownloadFromWv(self):
        idwell_name_dict = self.RequestWellHeader()
        well_name_id_queried = self.ParseInputParams(idwell_name_dict)
        self.RequestWellData(well_name_id_queried)

if __name__=='__main__':
    api=API('****Sample Well')
    api.Main_DownloadFromWv()
