import re #Module used to handle RegEx (Regular Expressions)
import requests #Module used to handle requests

class QueryArgGis():
    def __init__(self,wellnameInput='ALL_WELLS'):
        self.wellnameInput=wellnameInput
        self.Json_Res = {}
        #self.Json_From_Arc_Dict={'Titan-038': {'objectid': 16637, 'name': 'Titan-038', 'e': 705490.69276468, 'n': 7102661.10235206, 'longitude': '149° 3\' 22.319" E', 'latitude': '26° 10\' 51.044" S', 'notes': 'Roma North Future Development Wells - C Frith', 'ddlong': 149.05619962, 'ddlat': -26.18084556, 'gl': None, 'created_user': 'AGS_EDITOR', 'created_date': 1621384921000, 'last_edited_user': 'AGS_EDITOR', 'last_edited_date': 1623192278000, 'created_us': None, 'created_da': None, 'last_edite': None, 'last_edi_1': None, 'lotplan': '2RP173977', 'landholder': 'CHARLES OLIVER FRITH', 'lease_name': 'WSGP - East', 'lease_num': 'PL 1022', 'geog_coord_sys_id': 'GDA2020_MGA_Zone_55', 'operator': 'Senex Energy Ltd.', 'field_name': 'Titan'}}
        #self.MapWellNameAndIdDict={'Titan-038':'16637'}
        self.Json_From_Arc_Dict ={}
        self.MapWellNameAndIdDict ={}
        self.IdAndNameDict={}
        self.URL_GetAllObjID='https://senex.esriaustraliaonline.com.au/arcgis/rest/services/Senex_All_Permits/Wells/MapServer/0/query?where=1%3D1&geometryType=esriGeometryEnvelope&spatialRel=esriSpatialRelIntersects&returnGeometry=false&returnTrueCurves=false&returnIdsOnly=true&returnCountOnly=false&returnZ=false&returnM=false&returnDistinctValues=false&returnExtentsOnly=false&f=json'
        self.URLForOneWellLayerZero = 'https://senex.esriaustraliaonline.com.au/arcgis/rest/services/Senex_All_Permits/Wells/MapServer/0/{0}?f=pjson'
        #self.URLForOneWellLayerZero = 'https://senex.esriaustraliaonline.com.au/arcgis/rest/services/Senex_All_Permits/Wells/MapServer/0/query?where=1%3D1&text=&objectIds={0}?&f=pjson'



    def ParseInputWellName(self):
        if self.wellnameInput == 'ALL_WELLS':
            wellnameInput = list(self.MapWellNameAndIdDict.keys())
        else:
            wellnameInput = self.wellnameInput.split(',')
        return wellnameInput

    # def QueryLayerZero(self,MappedObjId,AWellName):
    #     URLForOneWellLayerZero = self.URLForOneWellLayerZero.format(str(MappedObjId))
    #     wellJson = requests.get(URLForOneWellLayerZero).json()
    #     if 'error' in wellJson.keys():
    #         print (MappedObjId,'-',AWellName,' DOES NOT Have any info returned')
    #     else:
    #         try:
    #             wellJson=wellJson['feature']['attributes']
    #         except KeyError:
    #             print (MappedObjId,'-',AWellName,' DOES NOT RETURN CORRECT JSON FORMAT - not starting with \'feature\'-\'attribute\'')
    #         return wellJson

    def QueryLayerZero(self): # this is for get the dict of {wellname:objid}
        All_Obj_ID = requests.get(self.URL_GetAllObjID).json()['objectIds']
        for objId in All_Obj_ID:
            WellName,wellJson = self.MapWellNameAndId(self.URLForOneWellLayerZero,objId)
            self.MapWellNameAndIdDict[WellName] = objId #{wellname:objid}
            self.Json_From_Arc_Dict[WellName] = wellJson
            #print (WellName)
        return self.MapWellNameAndIdDict,self.Json_From_Arc_Dict  #get {wellname:objid}, {wellname:table contents}

    def MapWellNameAndId(self,URLForOneWellLayerZero,ObjId):
        #print (URLForOneWellLayerZero.format(ObjId))
        wellJson = requests.get(URLForOneWellLayerZero.format(ObjId)).json()
        #print (wellJson['features']['attributes'])
        # for attributes in wellJson['features']:
        try:
            WellName = wellJson['feature']['attributes']['name']
            return WellName,wellJson['feature']['attributes']
        except KeyError:
            print (str(ObjId) + ' DO NOT HAVE Element \'name\'')

    def CreateJsonDict(self,wellnameInput):
        for AWellName in wellnameInput:
            if AWellName in self.MapWellNameAndIdDict.keys():
                print (AWellName,' Found in ArcGis')
                json_body=self.Json_From_Arc_Dict[AWellName]
                self.Json_Res[AWellName] = json_body
            else:
                print(AWellName, "-Well Name Input IS NOT EXISTED in ArcGis! Please Check.")
        return self.Json_Res

    def PrepareJsonLoadedToWellHeader(self,Json_Res):
        Required_Columns={'name':'wellname','operator':'operator','lease_name':'lease','lease_num':'LeaseCode','field_name':'fieldname','ddlong':'longitude','ddlat':'latitude','e':'utmx','n':'utmy','geog_coord_sys_id':'LatLongDatum','gl':'elvground'}
        Prepared_Json = {}
        for k,v in Json_Res.items():
            if v == None:
                print (k,' - No Table contents returned from ArcGis.')
            else:
                Prepared_Json[k]={}
                for arc_column,wv_column in Required_Columns.items():
                    try:
                        arc_value=v[arc_column.lower()]
                        Prepared_Json[k][wv_column]=arc_value

                    except Exception as e:
                        print (e,'- DOES NOT Exist, assign blank.')
                        Prepared_Json[k][wv_column] = ''

        return Prepared_Json

        #print(wellObj['feature']['attributes'])  # This should be the data you're looking for xoxo
    def Main(self):
        mapping_dict, Json_From_Arc_dict = self.QueryLayerZero()
        #wellNameInput = self.ParseInputWellName()
        #print('Well(s) input: ',wellNameInput)
        #Json_res = self.CreateJsonDict(wellNameInput)  # get {WellNameInput1:{tableContents},WellNameInput2:{tableContents}...}
        #print(Json_res)
        Prepared_Json = self.PrepareJsonLoadedToWellHeader(Json_From_Arc_dict)
        print(Prepared_Json)
        return Prepared_Json
if __name__ =='__main__':
    a=QueryArgGis('Titan-038,Titan-049')
    a.Main()
    # mapping_dict,Json_From_Arc_dict = a.QueryLayerZero()
    # wellNameInput = a.ParseInputWellName()
    # print (wellNameInput)
    # Json_res=a.CreateJsonDict(wellNameInput) #get {WellNameInput1:{tableContents},WellNameInput2:{tableContents}...}
    # print (Json_res)
    # Prepared_Json = a.PrepareJsonLoadedToWellHeader(Json_res)
    # print (Prepared_Json)
"""
#First we need to transform the given wellname into a format for the URL. ArcGIS seems to name Wells without a dash ("-") character, for example "Atlas-4" in Wellview is "Atlas 4" in ArcGIS. As we will be using the name in a URL, we use a "+" to represent a space, therefore in this case we can simply replace the "-" with a "+" to get the appropriately formatted string
origWellName = 'Atlas-5'
#cleanWellName = re.sub("-", "+", origWellName)
cleanWellName=origWellName
print(cleanWellName) #Should read "Atlas+4"

#Build the URL to query ArcGIS for a list of ArcGIS ID's WHERE name = Well Name
urlForId = 'https://senex.esriaustraliaonline.com.au/arcgis/rest/services/Senex_All_Permits/Wells/MapServer/1/query?where=senex_well_name+%3D+%27' + cleanWellName + '%27&text=&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=&returnGeometry=false&returnTrueCurves=false&maxAllowableOffset=&geometryPrecision=&outSR=&returnIdsOnly=true&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&returnDistinctValues=false&resultOffset=&resultRecordCount=&queryByDistance=&returnExtentsOnly=false&datumTransformation=&parameterValues=&rangeValues=&f=json'
print (urlForId)
#Query Arc for the Well ID and store the resulting JSON object "objectIds"
idJson = requests.get(urlForId)
print (idJson)
idObj = idJson.json()["objectIds"]
print (idObj[0])
#Strip the brackets surrounding the ArcGIS Well ID. For example, The ArcGIS ID for Atlas 4 is "18019". In our JSON object, this is returned as "[18019]"

#Same as before, we now build the second URL to query ArcGIS for a given well by its ArcGIS ID
urlForWell = 'https://senex.esriaustraliaonline.com.au/arcgis/rest/services/Senex_All_Permits/Wells/MapServer/1/' + arcId +'?f=pjson'

##Query Arc for the Well Information and store the entire resulting JSON object
wellJson = requests.get(urlForWell)
wellObj = wellJson.json()

print(wellObj['feature']['attributes']) #This should be the data you're looking for xoxo
"""