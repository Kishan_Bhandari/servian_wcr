from APIConnectToWV import API
from arcWellToJson import QueryArgGis
import time
import json
#welllist='Atlas-1,Atlas-2,Atlas-3,Atlas-4,Atlas-5,Atlas-6,Atlas-7,Atlas-8,Atlas-9,Atlas-10,Atlas-11,Atlas-12,Atlas-13,Atlas-14,Atlas-15,Atlas-16,Atlas-17,Atlas-18,Atlas-19,Atlas-20,Atlas-21,Atlas-22,Atlas-24,Atlas-25,Atlas-26,Atlas-27,Atlas-28,Atlas-29,Atlas-30,Atlas-31,Atlas-33,Atlas-34,Atlas-35,Atlas-36,Atlas-37,Atlas-38,Atlas-39,Atlas-40,Atlas-41,Atlas-42,Atlas-43,Atlas-60,Atlas-61,Atlas-62,Atlas-63,Eos-16,Eos-17,Eos-18,Eos-19,Eos-21,Eos-22,Eos-23,Eos-24,Eos-25,Eos-26,Eos-27,Eos-29,Eos-30,Eos-34,Eos-35,Glenora-25,Glenora-26,Glenora-27,Glenora-28,Glenora-29,Glenora-30,Glenora-31,Glenora-32,Glenora-33,Glenora-34,Glenora-43,Glenora-44,Glenora-45,Glenora-46,Glenora-47,Glenora-48,Glenora-49,Glenora-50,Glenora-59,Glenora-61,*COPY* Atlas-38,****Sample Well-P&A'
class Main_Process():
    def __init__(self,wellnamelist='ALL_WELLS'):
        self.wellnamelist = wellnamelist
        self.WvAPI = API(self.wellnamelist)
        self.ArgGis = QueryArgGis()#'Titan-038,Titan-049'
# welllist='****Sample Well'
    def GetFromArc(self):
        #ActualWellDataFromArcGis = 'Titan-038,Titan-049'
        Arc_Prepared_Json = self.ArgGis.Main()  # get well data of input well(s) from ArcGis
        return Arc_Prepared_Json
    def GetFromWv_NameAndID(self):
        WV_well_name_id_Dict = self.WvAPI.RequestWellHeader()  # get a dict like {wellname1:idwell1,wellname1:idwell1,...}
        return WV_well_name_id_Dict
    def MatchArcToWv(self,Arc_Prepared_Json,WV_well_name_id_Dict):

        Arc_Prepared_Json=Arc_Prepared_Json
        print (Arc_Prepared_Json)
        print ("---------------------------------------------line-----------------------------------------------------")
        WV_well_name_id_Dict=WV_well_name_id_Dict
        # -------------------------------------------------TESTING STARTS---------------------------------------------------#
        # Fake Json pulled from ArcGis (values are from 'Titan-038,Titan-049'):
        # Arc_Prepared_Json = {'****Sample Well': {'wellname': '****Sample Well', 'operator': 'Senex Energy Ltd09090.',
        #                                          'lease': 'WSGP - EastTest1', 'LeaseCode': 'PL 1022@',
        #                                          'fieldname': 'TitanDerek', 'longitude': 149.05619962,
        #                                          'latitude': -26.18084556, 'utmx': 705490.69276468,
        #                                          'utmy': 7102661.10235206, 'LatLongDatum': 'GDA2020_MGA_Zone_55',
        #                                          'elvground': None},
        #                      '****Sample Well-P&Atest': {'wellname': '****Sample Well-P&A',
        #                                                  'operator': 'Senex Energy Ltd.', 'lease': 'WSGP - East',
        #                                                  'LeaseCode': 'PL 1022', 'fieldname': 'Titan',
        #                                                  'longitude': 149.07496623, 'latitude': -26.19059731,
        #                                                  'utmx': 707349.40254749, 'utmy': 7101550.84207501,
        #                                                  'LatLongDatum': 'GDA2020_MGA_Zone_55', 'elvground': None}}
        # --------------------------------------------------TESTING FINISH----------------------------------------------------#
        delete_keys = []
        for wellname, wellJson in Arc_Prepared_Json.items():
            if wellname in WV_well_name_id_Dict.keys():
                Arc_Prepared_Json[wellname]['idwell'] = WV_well_name_id_Dict[wellname]
                print(wellname, " - Ready to Upload")
            else:
                delete_keys.append(wellname)
        for key in delete_keys:
            del Arc_Prepared_Json[key]
            print("ArcGis well - ", key, " - DOES NOT EXIST in WELLVIEW, Remove From the Queue")
        return Arc_Prepared_Json
    def PushArcToWv(self,Json_Ready_To_Upload):
        #idwellFile = self.WvAPI.getidwellfile()
        for wellname, wellJson in Json_Ready_To_Upload.items():
            #WVbearerToken = self.WvAPI.createToken(idwellFile)
            print ("----",wellJson)
            try:
                self.WvAPI.UpdateWellViewData('wvWellHeader', json.dumps(wellJson))
            except Exception as e:
                if hasattr(e, 'message'):
                    print(e.message)
                else:
                    print(e)
    def Timer(self,Func):
        start = time.time()
        Func()
        end = time.time()
        print('Time elapsed: ',end - start)
    def Main(self):
        start_super = time.time()
        start = time.time()
        print ("Requesting All Wells From ArcGis...")
        Arc_Prepared_Json = self.GetFromArc()
        print("ArcGis Request Finished")
        end = time.time()
        print('Time elapsed: ', end - start)
        start = time.time()
        print("Requesting WellId and Well Name From Wv...")
        WV_well_name_id_Dict = self.GetFromWv_NameAndID()
        print("WellView Request Finished")
        end = time.time()
        print('Time elapsed: ', end - start)
        start = time.time()
        print("Mapping Wells From ArcGis to WellView")
        Json_Ready_To_Upload = self.MatchArcToWv(Arc_Prepared_Json,WV_well_name_id_Dict)
        print("Mapping Finished")
        end = time.time()
        print('Time elapsed: ', end - start)

        start = time.time()
        print ('Pushing ArcGis to Wellview')
        self.PushArcToWv(Json_Ready_To_Upload)
        print('Pushing Finished')
        end = time.time()
        print('Time elapsed: ', end - start)

        print('Process Done')
        end_super=time.time()
        print('Total Time elapsed: ', end_super - start_super)
if __name__ == '__main__':
    start=Main_Process('Atlas-32')
    start.Main()
"""
Manually change well name Titan-038 and Titan-049 to ****Sample Well and ****Sample Well-P&A, only for testing.
if succeeds, ****Sample Well and ****Sample Well-P&A in wellview should be updated to values in Titan-038 and Titan-049
"""


