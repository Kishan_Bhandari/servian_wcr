from openpyxl import load_workbook
import json
import configparser
import warnings
import pandas as pd
import pyodbc
import re
from APIConnectToWV import API
warnings.simplefilter("ignore")
class SubSurfaceRawData():
    def __init__(self):
        self.excel_location = 'C:\\Users\\Derek.Liu\\Downloads\\Reservoir Management Database Working_TESTING_2.xlsx'
        self.wb = load_workbook(self.excel_location,data_only=True)
        self.sheetName = (self.wb.sheetnames)  # List of sheet (tab) names
        self.config=configparser.ConfigParser()
        self.config.read("./SS_Config.ini")
        self.conn,self.cursor = self.ConnectToDB()
        self.Sql_String = 'SELECT TableColumns FROM [Well_Completion_Reports].dbo.PelotonTableReflection' + ' ' + 'where TableName = ' + '\'' + '{}' + '\''
        self.Sql_PushToWellBoreFormation = 'SELECT * FROM [Well_Completion_Reports].dbo.PushToWV_wvWellboreFormation'
        self.Sql_PushToReservoir = 'SELECT * FROM [Well_Completion_Reports].dbo.PushToWV_wvWellboreReservoir'
        self.Sql_PushToPickOth = 'SELECT * FROM [Well_Completion_Reports].dbo.PushToWV_wvWellboreFormationPickOth'  ###
        self.WVAPI = API()
    def ConnectToDB(self):
        conn = pyodbc.connect('Driver={SQL Server};'
                              'Server=SENAZESQL01;'
                              'Database=Well_Completion_Reports;'
                              'Trusted_Connection=yes;')

        cursor = conn.cursor()
        return conn, cursor
    def Read_SS_FromConfiTable(self):
        DBTablesColumns={}
        for tab in self.config.sections():
            print(tab)
            DBColumns = pd.read_sql(self.Sql_String.format(self.config[tab]['DBTableName']),self.conn).values.tolist()
            if len(DBColumns) > 0:
                DBTablesColumns[tab] = ''.join(DBColumns[0]).split(',')
        return DBTablesColumns
    def ReadFromSS_Spreadsheet(self):
        TabsDataJson = {} #Our final master JSON object
        pd.set_option('display.max_columns', None)
        for tab in self.config.sections():
            sheetJson = []  # Blank Array to hold each row individually as a JSON object
            if self.config[tab]['Pop'] == 'Y':
                ws = self.wb[tab]
                for row in ws.iter_rows(min_row=int(self.config[tab]['Row_Start']), max_row=ws.max_row):
                    rowJson = {} #Blank Object to hold the rows JSON
                    l = 1 #Counting columns as we move through each cell - needed to get the field name from row 1
                    for cell in row:
                        fieldName = ws.cell(row = 1, column = l).value
                        #print (tab,'-',fieldName)
                        l += 1
                        if fieldName != None:
                            fieldName=re.sub('%','percent',re.sub('[/-]','_',re.sub(r'[\s?,]','',re.sub(r'\((.*)\)',r'_\1',fieldName))))
                            rowJson[fieldName] = str(cell.value)
                        else:
                            pass
                    sheetJson.append(rowJson)
            TabsDataJson[tab] = sheetJson #Store each sheets nested JSON object under an object with the name of the sheet in the master JSON object
        return TabsDataJson

    def InserIntoDB(self,TableName,ColNames,ColData):
        insert_sql = "insert into [Well_Completion_Reports].dbo." + TableName + " (" + ColNames + ") values (" + ColData + ")"
        self.cursor.execute (insert_sql)
        self.cursor.commit()
        return insert_sql
    def WashColData(self,row):
        WashColData="'"+re.sub("\^", "','", "^".join(list(row.values())).replace('\'', '\'\''))+"'"
        #print (WashColData)
        return WashColData


    def LoadTabToDB(self,TabsDataJson):
        #Table_Columns = ''.join(pd.read_sql(self.Sql_String.format(self.config['Prog Tops']['DBTableName']), self.conn).values.tolist()[0]).split(',')
        for tab in self.config.sections():
            for row in TabsDataJson[tab]:
                ColNames=','.join(list(row.keys()))
                ColData=self.WashColData(row)
                if len(set(ColData.split(','))) == 1 and list(set(ColData.split(',')))[0] == '\'None\'':
                    pass
                else:
                    n = self.InserIntoDB(self.config[tab]['DBTableName'],ColNames,ColData)
                    print(n)
    def DeleteFromTables(self):
        for tab in self.config.sections():
            delete_sql = "delete from [Well_Completion_Reports].dbo." + self.config[tab]['DBTableName']
            self.cursor.execute(delete_sql)
            self.cursor.commit()

    def GetDB_WellBoreFormationData(self):
        WBF_Data = pd.read_sql(self.Sql_PushToWellBoreFormation, self.conn).to_dict('records')
        for i in WBF_Data:
            print(json.dumps(i))
            if i['idrec'] == None:
                #print(json.dumps(i))
                self.WVAPI.POSTWellViewData('wvWellboreFormation',json.dumps(i))
                #pass
            else:
                #print(json.dumps(i))
                self.WVAPI.UpdateWellViewData('wvWellboreFormation',json.dumps(i))
                #pass

    def GetDB_ReserviorData(self):
        RES_Data = pd.read_sql(self.Sql_PushToReservoir, self.conn).to_dict('records')
        for i in RES_Data:
            self.WVAPI.UpdateWellViewData('wvWellboreReservoir',json.dumps(i))

    def GetDB_PickOth(self):  ###
        PickOth_Data = pd.read_sql(self.Sql_PushToPickOth, self.conn).to_dict('records')
        for i in PickOth_Data:
            print (json.dumps(i))
            self.WVAPI.POSTWellViewData('wvWellboreFormationPickOth', json.dumps(i))
    def Main_Ingest_SS(self):

        TabsDataJson = self.ReadFromSS_Spreadsheet()
        #x=self.Read_SS_FromConfiTable()
        self.DeleteFromTables()
        self.LoadTabToDB(TabsDataJson)

        self.GetDB_WellBoreFormationData()
        self.GetDB_ReserviorData()
        self.GetDB_PickOth()  ###
#Basic Well Info:Well Class,Well Purpose,Basin,Target Strat,Target Resource
#Other Operational:FRT, Testing Reservoir,Walloon Thickness,Net Coal (m)
#Actual Tops:Actual Formation,Actual Top Depth (mMD)
#Prog Other:Density Cutoff

if __name__=='__main__':
    SS = SubSurfaceRawData()
    SS.Main_Ingest_SS()