from APIConnectToWV import API
from DBReadLoadUpdate import DBReadLoadUpdate
class Main_SyncWvToSQLDB():
    def __init__(self,wellnames_passed_in='****Sample Well,****Sample Well-P&A'): #change default to 'ALL_WELL' if recognize no input as All wells
        # if no specific well name input, all wells will be downloaded which is time consumed, please consider if it is necessary!
        self.API = API(wellnames_passed_in)
        self.MergeToDB = DBReadLoadUpdate()
    def Main(self):
        self.API.Main_DownloadFromWv()
        self.MergeToDB.Main()
    def Run(self):
        self.Main()
if __name__ == '__main__':
    a= Main_SyncWvToSQLDB()
    a.Run()