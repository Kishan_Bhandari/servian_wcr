# Author: Kishan Bhandari  
# Date: 21/07/21
# Description: To establish a connection to the Office365 REST API, authenticate, 
# retrieve and read a file stored on SharePoint.

# --- Imports --- #
import os
import socket
from office365.runtime.auth.client_credential import ClientCredential
from office365.sharepoint.files.file import File
from office365.directory.user import User

# # Azure AD oAuth for the well completion report app attached to kishan.bhandari@senexenergy.com.au
# app_client_ID = '0020adb5-8719-4adf-9163-f1a289c8eca9'
# ### 
# # DO NOT DELETE OR MODIFY THIS VALUE #
# app_client_scrt_value = 'J.0VbfH2ws6dO8fCUMcFifl-._2B8znxYo' 
# ###
# app_client_scrt_ID = '0de8e3a8-a46f-453d-8d0e-d776aceeb6da'
# tenant_ID = 'bc24e539-4c89-4c5e-b9e5-1fa38d9f6921'

# --- Global variables --- #
team_site_url = "https://senexenergy.sharepoint.com/"

# Azure AD app-only principal credentials ID and secret generated, and 'Read' permissions granted following the steps outlined here:
# https://docs.microsoft.com/en-us/sharepoint/dev/solution-guidance/security-apponly-azureacs
# Right = 'Read' for these credentials.
Credentials = {
    'client_id': 'ef35517e-6407-4084-a416-06a2060705e9',
    'client_secret': '5erFxCFtmAfRHkyNXLk0cU8W4MyniZlJYVDoblHepCU=',
}

def getUsers():
    User.contacts()

# --- Download a file from the SharePoint site: OpsDevelopmentPlanning. --- #
def downloadSubsurfaceFile(fileName):

    client_credentials = ClientCredential(Credentials['client_id'],Credentials['client_secret'])
    
    # URL for file location on SharePoint online.
    abs_file_url = "{site_url}sites/OpsDevelopmentPlanning/Shared%20Documents/20%20Surat%20Development/Subsurface%20Operations/CY21%20Campaign/{file_name}".format(site_url=team_site_url, file_name=fileName)

    # Use different slash depending on windows or linux environment
    if socket.gethostname() == 'senazedigitalprod01' or socket.gethostname() == 'senazewvinttest01':
        local_path = os.getcwd() + '/SubsurfaceFiles'
    else:
        local_path = os.getcwd() + '\SubsurfaceFiles'
    
    # Download the subsurface file from SharePoint into a local folder.
    # local_path = os.getcwd() + '/SubsurfaceFiles'
    file_name = os.path.basename(abs_file_url)
    with open(os.path.join(local_path, file_name), 'wb') as local_file:
        file = File.from_url(abs_file_url).with_credentials(client_credentials).download(local_file).execute_query()
    print("'{0}' file has been downloaded into {1}".format(file.serverRelativeUrl, local_file.name))

    # Return path of downloaded file.
    return local_file.name

if __name__ == "__main__":
    # fileName = 'Reservoir Management Database Working_TESTING.xlsx' # Name of file in OpsDevelopmentPlanning to download.
    # downloadSubsurfaceFile(fileName)
    getUsers()