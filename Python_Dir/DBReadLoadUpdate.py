
import json
import pandas as pd
import pyodbc
import os
import re
import hashlib
pd.set_option('display.max_columns',None)
# df=pd.read_json(r"C:\Users\Derek.Liu\Downloads\map_wellview_api_data_to_completion_format\map_wellview_api_data_to_completion_format\wellview_data\JsonFiles_Atlas-01\DatawvWellheader.js")
# print (df)
# headerList=['WELL_NAME','OPERATOR','Test123']
# df1=pd.DataFrame(columns=headerList)
# header_dict={'idwell':'well_identification','basin':'test123','derekliu':'xiaozeliu'}
# df.rename(columns=header_dict,inplace=True)
# print (df)
# df.to_csv(r"C:\Users\Derek.Liu\Downloads\map_wellview_api_data_to_completion_format\map_wellview_api_data_to_completion_format\wellview_data\JsonFiles_Atlas-01\DatawvCas.csv",index=None)
class DBReadLoadUpdate():
    def __init__(self):
        self.conn=pyodbc.connect('Driver={SQL Server};'
                              'Server=SENAZESQL01;'
                              'Database=Well_Completion_Reports;'
                              'Trusted_Connection=yes;')
        self.cursor=self.conn.cursor()

    def MergeDBTables(self,Merge_Dict):
        error_msg = []

        for k,v in Merge_Dict.items():
            exec_sp_MergeTables="exec Well_Completion_Reports.dbo.MergeTables @TableName='" + k + "',@UpdateColumns='" + v[0][:-1] + "',@InsertColumns='"+v[1] + "'" + ",@OutputTargetColumns='" + v[2][:-1] + "'" #Merge_Update_Col_Str[:-1] is aiming to remove the comma at the end of the string
            exec_sp_DropTempTables = "exec Well_Completion_Reports.dbo.DropTempTables @TableName='" + k +"'"
            try:
                self.cursor.execute(exec_sp_MergeTables)
                self.cursor.commit()
                print (k," -- Merged")
            except Exception as e:
                print (k,' -- has merging error')
                error_msg.append(str(e))
                print (error_msg)
                pass

            try:
                self.cursor.execute(exec_sp_DropTempTables)
                self.cursor.commit()
                print("temp table ", k, " --dropped")
            except Exception as e:
                print('temp table ', k, " -- dropped error")
                error_msg.append(str(e))
                print(error_msg)
                pass
    def UploadJsonToDB(self,df_to_upload,table_name):
        '|'.join(df_to_upload.columns.tolist())
        temp_table='temp_'+table_name
        error_msg = []
        Merge_Update_Col_Str = ''
        Merge_OUTPUT_Target_Str = ''
        Question_Mark = ',?'  # placeholder prepared for creating the insert statement
        Question_Mark = (Question_Mark * len(df_to_upload.columns.tolist()))[1:]
        column_list=df_to_upload.columns.tolist()
        #insert_column_list = ','.join(df_to_upload.columns.tolist())
        insert_temp_col_list =[]
        for x in column_list:
            insert_temp_col_list.append('['+x+']')
        insert_temp_col_list=','.join(insert_temp_col_list)
        for i in column_list:
            column_list[column_list.index(i)]='['+i+']' + ' varchar(2000)'
            Merge_Update_Col_Str += 'target.[' + i + ']=source.[' + i + '],'
            #Merge_OUTPUT_Target_Str += "case when $action=''INSERT'' then INSERTED.[" + i + "] else DELETED.[" + i + "] end,"
            Merge_OUTPUT_Target_Str += "case when $action=''INSERT'' then INSERTED.[" + i + "] else '' end,"
        create_temp_table_columns=','.join(column_list)
        insert_temp_sql = 'insert into [Well_Completion_Reports].dbo.' + temp_table + ' (' + insert_temp_col_list + ') values (' + Question_Mark + ')'
        exec_sp_temptb_create="exec Well_Completion_Reports.dbo.CreateTempLandingTable @TableName='temp_" + table_name +"',@Columns='"+create_temp_table_columns +"'"
        self.cursor.setinputsizes([(pyodbc.SQL_WLONGVARCHAR, 500, 100)])
        if len(list(df_to_upload.itertuples(index=False, name=None)))>0:
            try:
                self.cursor.execute(exec_sp_temptb_create)
                self.cursor.commit()
                print (temp_table,"temp table created")
                self.cursor.executemany(insert_temp_sql, list(df_to_upload.itertuples(index=False, name=None)))
                self.cursor.commit()
                print(temp_table, "temp table inserted")
            except Exception as e:
                #print ("error 1")
                print(temp_table,'error')
                error_msg.append(str(e))
                pass


        print ("upload function finished, here is the error log: ", error_msg)
        return table_name,Merge_Update_Col_Str,insert_temp_col_list,Merge_OUTPUT_Target_Str
    def ReadConfigTable(self):

        Json_Location='I:\\00. Temporary File Dump Location\\Wellview API\\JsonFilesNewAPITest\\'
        #Json_Location='C:\\Users\\Derek.Liu\\Downloads\\JsonFiles_new\\JsonFiles\\'
        """
        for migration:
        Json_Location='a Azure URL location where storing JSON files', but need to investigate if that URL can be fitted into to os.listdir()
        """
        p=os.listdir(Json_Location)
        self.cursor.fast_executemany = True
        Merge_Dict = {}
        for JsonFolder in p:
            if JsonFolder != '.idea':
            #if JsonFolder == 'JsonFiles_Sample Well':
                json_list_for_each_well=os.listdir(Json_Location+JsonFolder)
                print(JsonFolder)
                for each_table in json_list_for_each_well:
                    df = pd.read_json(Json_Location+JsonFolder+'\\'+each_table)
                    table_name=re.match(r'Data(wv.*).jso*n*',each_table).group(1)
                    #if table_name in ['wvWellboreFormation']:

                    Sql_String = 'SELECT TableColumns FROM [Well_Completion_Reports].dbo.PelotonTableReflection' + ' ' + 'where TableName = ' + '\'' + table_name + '\''
                    Table_Columns=''.join(pd.read_sql(Sql_String,self.conn).values.tolist()[0]).split(',')
                    df_to_upload = pd.DataFrame(columns=Table_Columns)
                    for column in Table_Columns:
                        if column.lower() in df.keys():
                            #df_to_upload[column] = df[column.lower()].fillna('*** - ' + column + ' field empty in Peloton')
                            df_to_upload[column] = df[column.lower()].fillna('')
                        else:
                            #df_to_upload[column] = ['*** - ' + column + ' field empty in Peloton']*len(df)
                            df_to_upload[column] = ''
                    df_to_upload = df_to_upload.applymap(str)
                    df_to_upload['hashkey'] = pd.Series((hashlib.md5('|'.join(list(row)).encode("utf-8")).hexdigest() for _, row in df_to_upload.iterrows()))
                    table_name,Merge_Update_Col_Str,insert_column_list,Merge_OUTPUT_Target_Str = self.UploadJsonToDB(df_to_upload,table_name)
                    Merge_Dict[table_name] = [Merge_Update_Col_Str,insert_column_list,Merge_OUTPUT_Target_Str]
        self.MergeDBTables(Merge_Dict)

        self.conn.close()
    def DeleteJsonFiles(self):
        import os
        import shutil
        for root, dirs, files in os.walk('./JsonFilesNewAPITest'):
            for f in files:
                os.unlink(os.path.join(root, f))
            for d in dirs:
                shutil.rmtree(os.path.join(root, d))
    def Main(self):
        self.ReadConfigTable()
        self.DeleteJsonFiles()
if __name__=='__main__':
    a=DBReadLoadUpdate()
    a.ReadConfigTable()
    a.DeleteJsonFiles()